<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('home', 'HomeController@index');
Route::get('cards', 'CardController@index');
Route::get('cards/create', 'CardController@create');
Route::post('cards', 'CardController@store');

Route::get('deck', 'DeckController@index');
Route::post('deck', 'DeckController@store');
Route::delete('deck', 'DeckController@destroy');