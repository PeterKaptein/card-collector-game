<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Card extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'image', 'title', 'description',
    ];

    public function users()
    {
        return $this->belongsToMany('App\User', 'card_user', 'card_id', 'user_id')->withTimestamps();
    }
}
