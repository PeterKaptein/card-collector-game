@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading box-header">
                        <h3 class="left-cell">Add new card</h3>
                    </div>
                    <div class="panel-body">
                        {{ Form::open(['url' => 'cards', 'files' => true]) }}
                        <div class="form-group">
                            {{ Form::label('file', 'Image:') }}
                            {{ Form::file('file') }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('title', 'Title:') }}
                            {{ Form::text('title', null, ['class' => 'form-control']) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('description', 'Description:') }}
                            {{ Form::textarea('description', null, ['class' => 'form-control']) }}
                        </div>
                        <div class="form-group">
                            {{ Form::submit('Add card', ['class' => 'btn btn-primary form-control']) }}
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection