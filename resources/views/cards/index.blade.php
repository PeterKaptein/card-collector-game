@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading box-header">
                        <h3 class="left-cell">Overview cards</h3>
                        {{--{{ HTML::link('/cards/create', 'Add card', ['class' => 'right-cell text-right'], true)}}--}}
                        <a href="{{ url('cards/create') }}" class="right-cell text-right"><button class="btn">Add card</button></a>
                        {{--<span class="right-cell text-right"><button class="btn">Add card</button></span>--}}
                    </div>
                    <div class="panel-body my-card-wrapper">
                        @foreach($cards as $card)
                            {{--<li class="list-group-item">--}}
                            {{--<h5>{{ $card->title }}</h5>--}}
                            {{--<div>{{ $card->image }}</div>--}}
                            {{--<div>{{ $card->description }}</div>--}}
                            {{--</li>--}}
                            <div class="panel panel-default my-card">
                                <img src="{{ url($card->image) }}" alt="Card image cap">
                                <div class="panel-body">
                                    <h4>{{ $card->title }}</h4>
                                    <p>{{ $card->description }}</p>
                                    {{--<card-button :card="{{ $card->id }}">--}}
                                        {{--<button class="btn btn-primary">Add to deck</button>--}}
                                    {{--</card-button>--}}
                                    {{ Form::open(['url' => 'deck', 'method' => 'store']) }}
                                    {{ Form::hidden('id', $card->id) }}
                                    @if(count($card->users))
                                        {{ Form::submit('Add to deck', ['class' => 'btn btn-primary', 'disabled']) }}
                                    @else
                                        {{ Form::submit('Add to deck', ['class' => 'btn btn-primary']) }}
                                    @endif
                                    {{ Form::close() }}
                                </div>
                            </div>
                        @endforeach
                        <div>{{ $cards->links() }}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection