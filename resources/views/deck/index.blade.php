@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading box-header">
                        <h3 class="left-cell">Overview deck</h3>
                    </div>
                    <div class="panel-body my-card-wrapper">
                        @foreach($cards as $card)
                            <div class="panel panel-default my-card">
                                <img src="{{ url($card->image) }}" alt="Card image cap">
                                <div class="panel-body">
                                    <h4>{{ $card->title }}</h4>
                                    <p>{{ $card->description }}</p>
                                    {{ Form::open(['url' => 'deck', 'method' => 'delete']) }}
                                    {{ Form::hidden('id', $card->id) }}
                                    {{ Form::submit('Remove from deck', ['class' => 'btn btn-danger']) }}
                                    {{ Form::close() }}
                                    {{--<a href="#" class="btn btn-danger">Remove from deck</a>--}}
                                </div>
                            </div>
                        @endforeach
                            <div>{{ $cards->links() }}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection